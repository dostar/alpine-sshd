#!/usr/bin/env sh

set -e

[ "${DEBUG}" = '1' ] && set -x

user_format_help()
{
    echo -n "User format: 'name:uid:gid', e.g. 'me:1000:1000'."
}

manage_keys()
{
    SSHD_KEYS_DIR=/etc/ssh/keys
    if [ -d ${SSHD_KEYS_DIR} ]; then
        echo "Using existing host keys in ${SSHD_KEYS_DIR}"
    else
        mkdir -p ${SSHD_KEYS_DIR}
        ssh-keygen -A
        echo "    To use custom host keys instead, you could mount -v\${HOME}/my_host_keys:${SSHD_KEYS_DIR}:ro"
        mv /etc/ssh/ssh_host_* ${SSHD_KEYS_DIR}
    fi
    for KEY_TYPE in dsa rsa ecdsa ed25519; do
        echo "    MD5 $(md5sum ${SSHD_KEYS_DIR}/ssh_host_${KEY_TYPE}_key)"
    done
}

manage_users()
{
    SSH_USERS=${SSH_USERS:?"Please define SSH_USERS, for example as -eSSH_USERS=user:1000:1000"}

    echo "CREATE_MAIL_SPOOL=no" >> /etc/default/useradd

    echo ${SSH_USERS} | tr ',' '\n' | while read SSH_USER; do
        echo "${SSH_USER}" | tr ':' ' ' | { 
            read _NAME _UID _GID _HOME
            _NAME=${_NAME:?"User name not defined in '${SSH_USER}'. $(user_format_help)"}
            _UID=${_UID:?"User UID not defined in '${SSH_USER}'. $(user_format_help)"}
            _GID=${_GID:?"User GID not defined in '${SSH_USER}'. $(user_format_help)"}
            _HOME=${_HOME:-/}

            echo "Adding user '${_NAME}' uid '${_UID}' gid '${_GID}' home ${_HOME}."
            if [ ! -f /etc/authorized_keys/${_NAME} ]; then
                cat <<EOM
    Without file '/etc/authorized_keys/${_NAME}', user '${_NAME}' will not be able to log in.
    Possible approaches:
        -v\${HOME}/.ssh/id_rsa:/etc/authorized_keys/${_NAME}:ro
        -v\${HOME}/users_keys:/etc/authorized_keys:ro
EOM
                return 1
            else
                echo "    MD5 $(md5sum /etc/authorized_keys/${_NAME})"
            fi

            (
                set -x
                /usr/sbin/groupadd -g ${_GID} ${_NAME} --non-unique
                /usr/sbin/useradd -u ${_UID} -g ${_GID} -s '' -p x ${_NAME} --home ${_HOME} --non-unique
            ) 2>&1 | sed -e 's/^/    /'
        }
    done
}

startup() 
{
    echo "Starting container '$(cat /etc/container-release)'."
}

main()
{
    startup
    manage_users
    manage_keys
    set -x; exec "$@"
}

main "$@"