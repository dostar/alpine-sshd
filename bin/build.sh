#!/bin/sh

PROJECT_ROOT="$( cd "$(dirname "$0")"/.. ; pwd -P )"
PROJECT_NAME=dostar/$(basename ${PROJECT_ROOT})

echo "Project ${PROJECT_NAME} in ${PROJECT_ROOT} - sha=${CI_COMMIT_SHORT_SHA}"

cd ${PROJECT_ROOT}/docker-context

ALPINE_BASE_TAG=3.8
GIT=${CI_COMMIT_SHORT_SHA-$(git log -n1 --format="%h")}

NAME_TAGS="${PROJECT_NAME} ${PROJECT_NAME}:${ALPINE_BASE_TAG} ${PROJECT_NAME}:${ALPINE_BASE_TAG}-${GIT}"

(
    _TAGS=$(echo " ${NAME_TAGS}" | sed -e 's/ / -t /g')
    set -x
    docker build \
        ${_TAGS} \
        --build-arg ALPINE_BASE_TAG=${ALPINE_BASE_TAG} \
        --build-arg FINGERPRINT="${PROJECT_NAME}:${ALPINE_BASE_TAG}-${GIT}" \
        .
)

for NAME_TAG in ${NAME_TAGS}; do
    (set -x; ${PUSH_PREFIX} docker push ${NAME_TAG})
done
