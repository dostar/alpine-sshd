# Alpine sshd

An Alpine-based image with sshd installed. See [Use with sshfs](#Use with sshfs) for details on how to expose a mounted volume.

Dockerhub repository: https://cloud.docker.com/u/dostar/repository/docker/dostar/alpine-sshd

The following container allows the host user to connect to it using the locally available `id_rsa` identity.

```bash
docker run -it --rm \
    -p 2340:22 \
    -e SSH_USERS=${USER}:$(id -u):$(id -g) \
    -v ${HOME}/.ssh/id_rsa.pub:/etc/authorized_keys/${USER}:ro \
    dostar/alpine-sshd
```

```bash
ssh ${USER}@localhost -p 2340 sh -c "id; pwd"
```

In production mode, you would probably want to use parameter `-d` instead of `-it`.

# Configuration

1. `SSH_USERS=user_1:uid_1:gid_1:home_1,user_2:uid_2:gid_2:home_2,...`

Mandatory. For each item in the comma-separated string, a user is created with the corresponding name, UID, and GID.
UIDs and GIDs need not be unique. The home directory is optional and defaults to `/`.

2. `-v path/to/id_rsa.pub:/etc/authorized_keys/user_i:ro` or `-v path/to/id_dir:/etc/authorized_keys:ro`

Mandatory. For each user `user_i` defined by the variable `SSH_USERS`, a public key file must be available at
`/etc/authorized_keys/user_i`.

The first variant shows how to map an individual public key file to a specific user. The second variant shows how
to map an entire directory containing all the public key files, one for each user.

3. `-v path/to/host-keys:/etc/ssh/keys:ro`

Optional. If this configuration is not present, the server generates its own host keys with `ssh-keygen -A`. If present,
the host keys available in the mapped directory are used.

# Use with sshfs

```bash
docker volume create sshd-vol
docker run -it --rm -v sshd-vol:/sshd-vol alpine sh -c "chown $(id -u) sshd-vol"
docker run -d --rm -eSSH_USERS=${USER}:$(id -u):$(id -g) \
    -p 2222:22 \
    -v ${HOME}/.ssh/id_rsa.pub:/etc/authorized_keys/${USER}:ro \
    -v sshd-vol:/sshd-vol \
    --name=sshd-server-1 \
    dostar/alpine-sshd

[ -d ${HOME}/sshd-vol ] || mkdir ${HOME}/sshd-vol

MY_SSH_OPTS="-o LogLevel=ERROR -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

ssh   ${MY_SSH_OPTS} -p 2222 localhost "hostname; ls -ld /sshd-vol"
sshfs ${MY_SSH_OPTS} -p 2222 localhost:/sshd-vol ${HOME}/sshd-vol -o volname=/sshd-vol

echo "Hi $(hostname) $(date)" >${HOME}/sshd-vol/test.txt
ssh   ${MY_SSH_OPTS} -p 2222 localhost "hostname; cat /sshd-vol/text.txt"
```

# Debugging

To start sshd in debug mode, use `/usr/sbin/sshd -D -d` as the command:

```bash
docker run -it --rm \
    -p 2340:22 \
    -e SSH_USERS=${USER}:$(id -u):$(id -g) \
    -v ${HOME}/.ssh/id_rsa.pub:/etc/authorized_keys/${USER}:ro \
    dostar/alpine-sshd /usr/sbin/sshd -D -d
```

# Use and error messages

The container prints helpful messages to assist with its proper configuration.

```text
$ docker run -it --rm dostar/alpine-sshd
Starting container 'dostar/alpine-sshd:3.8-e2cebe9e'.
/usr/bin/entrypoint.sh: line 76: SSH_USERS: Please define SSH_USERS, for example as -SSH_USERS=user:1000:1000
```

```text
$ docker run -it --rm -eSSH_USERS=user:1000:1000 dostar/alpine-sshd
Starting container 'dostar/alpine-sshd:3.8-e2cebe9e'.
Adding user 'user' uid '1000' gid '1000'.
    Without file '/etc/authorized_keys/user', user 'user' will not be able to log in.
    Possible approaches:
        -v${HOME}/.ssh/id_rsa:/etc/authorized_keys/user:ro
        -v${HOME}/users_keys:/etc/authorized_keys:ro
```

```text
$ docker run -it --rm -eSSH_USERS=user:1000:1000 \
    -v${HOME}/.ssh/id_rsa:/etc/authorized_keys/user:ro \
    dostar/alpine-sshd
Starting container 'dostar/alpine-sshd:3.8-e2cebe9e'.
Adding user 'user' uid '1000' gid '1000'.
    MD5 57033f7c9bd629cb631de623f71a8a9f  /etc/authorized_keys/user
    + /usr/sbin/groupadd -g 1000 user --non-unique
    + /usr/sbin/useradd -u 1000 -g 1000 -s  -p x user --non-unique
ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519 
    To use custom host keys instead, you could mount -v${HOME}/my_host_keys:/etc/ssh/keys:ro
    MD5 01135c78e9e20d89e7842fdc6d129bd1  /etc/ssh/keys/ssh_host_dsa_key
    MD5 b8eaeba33068a296f30c506e2a040f60  /etc/ssh/keys/ssh_host_rsa_key
    MD5 c330741aba11f1b1ed0e568784d1bdee  /etc/ssh/keys/ssh_host_ecdsa_key
    MD5 8ec50c3704515615e5a62d064561360c  /etc/ssh/keys/ssh_host_ed25519_key
+ exec /usr/sbin/sshd -D
```

```text
$ docker run -it --rm -eSSH_USERS=user:1000:1000 \
    -v${HOME}/test_user_keys_dir:/etc/authorized_keys:ro \
    -v${HOME}/test_host_keys_dir:/etc/ssh/keys:ro \
    dostar/alpine-sshd
Starting container 'dostar/alpine-sshd:3.8-e2cebe9e'.
Adding user 'user' uid '1000' gid '1000'.
    MD5 57033f7d9bd629bb621d7623f71a8a9f  /etc/authorized_keys/user
    + /usr/sbin/groupadd -g 1000 user --non-unique
    + /usr/sbin/useradd -u 1000 -g 1000 -s  -p x user --non-unique
Using existing host keys in /etc/ssh/keys    
    MD5 01135c78e9e20d89e7842fdc6d129bd1  /etc/ssh/keys/ssh_host_dsa_key
    MD5 b8eaeba33068a296f30c506e2a040f60  /etc/ssh/keys/ssh_host_rsa_key
    MD5 c330741aba11f1b1ed0e568784d1bdee  /etc/ssh/keys/ssh_host_ecdsa_key
    MD5 8ec50c3704515615e5a62d064561360c  /etc/ssh/keys/ssh_host_ed25519_key
+ exec /usr/sbin/sshd -D
```
